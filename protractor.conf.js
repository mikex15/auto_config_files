'use strict';

 //var paths = require('./.yo-rc.json')['generator-gulp-angular'].props.paths;

// An example configuration file.
exports.config = {
  // The address of a running selenium server.
  //seleniumAddress: 'http://localhost:4444/wd/hub',
  //seleniumServerJar: deprecated, this should be set on node_modules/protractor/config.json

  // Capabilities to be passed to the webdriver instance.
  multiCapabilities:  [{
    browserName: 'chrome'
	//browserName: 'firefox'
  }],

  // Spec patterns are relative to the current working directly when
  // protractor is called.
  //specs: ['demo.spec.js'],
  //specs: ['notification.spec.js'],
  //specs: ['verifyemail2.spec.js'],
  specs: ['accountdetails2.spec.js'],

  // Options to be passed to Jasmine-node.
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 120000,
    isVerbose: true,
    includeStackTrace: true
  }
};

/*
exports.config = {
  //seleniumServerJar: 'selenium/selenium-server-standalone-2.45.0.jar',
  //chromeDriver: 'selenium/chromedriver',
  // with multiCapabilities protractor will start several instances or as many as defined below
  multiCapabilities: [

    // mobile
    {
      browserName: 'chrome',
      chromeOptions: {
        // set the userAgent to be iPhone
        //args: ['--user-agent="Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X; en-us) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465 Safari/9537.53"']
		args: ['--user-agent="Mozilla/5.0 (Linux; <Android Version>; <Build Tag etc.>) AppleWebKit/<WebKit Rev> (KHTML, like Gecko) Chrome/<Chrome Rev> Mobile Safari/<WebKit Rev>"']
      },
      specs: [
        //'notification.spec.js'
		'sendcreditpostkyc.spec.js'
      ]
    },
  ],
  // set the max instances you want to run at the same time
  //maxSessions: 1,
  //baseUrl: 'http://localhost:8000'

  // Spec patterns are relative to the current working directly when
  // protractor is called.
  //specs: ['demo.spec.js'],
  //specs: [''],
  //specs: ['recoverpassword.spec.js'],

    // Options to be passed to Jasmine-node.
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 90000,
    sVerbose: true,
    includeStackTrace: true
  }

};
*/
/*
exports.config = {
  // The address of a running selenium server.
  //seleniumAddress: 'http://localhost:4444/wd/hub',
  //seleniumServerJar: deprecated, this should be set on node_modules/protractor/config.json

  // Capabilities to be passed to the webdriver instance.
    multiCapabilities: [{
    'browserName': 'chrome',
    'specs': ['login.spec.js']
  }, {
    'browserName': 'chrome',
    'specs': ['signup.spec.js']
  }],

  // Spec patterns are relative to the current working directly when
  // protractor is called.
  //specs: ['demo.spec.js'],
  //specs: ['notification.spec.js'],
  //specs: ['history.spec.js'],
  //specs: ['verifymobile.spec.js'],

  // Options to be passed to Jasmine-node.
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 120000,
    sVerbose: true,
    includeStackTrace: true
  }
};
*/