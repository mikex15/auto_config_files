'use strict';

var paths = require( './.yo-rc.json' )[ 'generator-gulp-angular' ].props.paths;
var istanbulPlugin = require( 'protractor-istanbul-plugin' );
var HtmlReporter = require( 'protractor-html-screenshot-reporter' );
require( 'jasmine-reporters' );
var reporter = new HtmlReporter( {
    baseDirectory: './e2e-results/', // a location to store screen shots.
    docTitle: 'Coverage details',
    docName: 'coverage.html'
} );
// An example configuration file.
exports.config = {
    // The address of a running selenium server.
    //seleniumAddress: 'http://localhost:4444/wd/hub',
    //seleniumServerJar: deprecated, this should be set on node_modules/protractor/config.json
    onPrepare: function () {
        require( 'jasmine-reporters' );
        jasmine.getEnv().addReporter( reporter );
        // var folderName = ( new Date() ).toString().split( ' ' ).splice( 1, 4 ).join( ' ' );
        // var mkdirp = require( 'mkdirp' );
        // var newFolder = "./reports/" + folderName;
        //
        //
        // mkdirp( newFolder, function ( err ) {
        //     if ( err ) {
        //         console.error( err );
        //     } else {
        //
        //     }
        // } );
    },
	getPageTimeout:120000,
    // Capabilities to be passed to the webdriver instance.
    capabilities: {
  'browserName': 'phantomjs',

  /* 
   * Can be used to specify the phantomjs binary path.
   * This can generally be ommitted if you installed phantomjs globally.
   */
  'phantomjs.binary.path': require('phantomjs').path,

  /*
   * Command line args to pass to ghostdriver, phantomjs's browser driver.
   * See https://github.com/detro/ghostdriver#faq
   */
  'phantomjs.ghostdriver.cli.args': ['--loglevel=DEBUG']
},

    plugins: [ {
        inline: istanbulPlugin,
        path: 'node_modules/protractor-istanbul-plugin',
        logAssertions: true,
        failAssertions: true
    } ],

    // Spec patterns are relative to the current working directly when
    // protractor is called.
    //specs: [ paths.e2e + '/**/*.js' ],
    //specs: [ paths.e2e + 'suspend.spec.js' ],
	specs: ['./e2e/postwhitelist3/*.*js' ],

    // Options to be passed to Jasmine-node.
    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 120000,
        isVerbose: true,
        includeStackTrace: true
    }
};

